#!/bin/bash

# sets k8s context so the user can do things with k8s manually
# usage: source <(curl -Ls https://gitlab.com/opentracing-workshop/ee-bootstrap/raw/master/get-k8s-creds.sh)

# check if user executed script directly or via source
(return 0 2>/dev/null) && sourced=1 || sourced=0

if [ $sourced -eq 0 ]; then
    echo "ERROR: executing this script in a sub-shell is not permitted, exiting"
    echo "ERROR: please execute this script using this command: > source ./setup-ee-workshop.sh <"
    echo "... exiting"
    return 1
fi

PAYLOAD="{\"username\": \"admin\", \"password\": \"admin1234\"}"
TOKEN=$(curl --insecure  -d "$PAYLOAD" -X POST https://localhost/auth/login  | jq -r ".auth_token")

curl -k -H "Authorization: Bearer $TOKEN" https://localhost/api/clientbundle > /tmp/bundle.zip
mkdir /tmp/certs-$TOKEN
pushd /tmp/certs-$TOKEN
unzip /tmp/bundle.zip
rm /tmp/bundle.zip
eval "$(<env.sh)"
popd

echo "... k8s context set, test with > kubectl get all <"