#!/bin/bash

# this script will setup all worker nodes in a ee-labs environment to run mixed workloads (k8s)
# USAGE: curl -Ls https://bit.ly/2DcQhRz | bash

# check if user is on a manager node, fail if not
docker node ls &>/dev/null
if [ $? -ne 0 ]; then
    echo "ERROR: detected worker node"
    echo "ERROR: please switch to > manager1 < and run this command again"
    echo "... exiting"
    exit 1
fi

echo "... updating worker nodes to run K8S workloads"

docker node update --label-add com.docker.ucp.orchestrator.kubernetes=true worker1
docker node update --label-add com.docker.ucp.orchestrator.kubernetes=true worker2
docker node update --label-add com.docker.ucp.orchestrator.kubernetes=true worker3

echo "... nodes have been updated"